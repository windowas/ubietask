<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = auth()->user();

        return view('home')->with('user', $user);
    }

    public function edit($id)
    {
      $user = User::find($id);
      // check for current user
      if(auth()->user()->id !== $user->id){
        return redirect('/')->with('error', 'Unauthorized Action');
      }
      return view('edit')->with('user', $user);
  }

  public function update(Request $request, $id)
  {
    $request->validate([
      'name' => 'required|string|max:255',
      'lname' => 'required|string|max:255',
      'country' => 'required|string',
      'city' => 'required|string',
      'email' => "required|string|email|max:255|unique:users,id".$request->id
    ]);
      $user = User::find($id);
      if(auth()->user()->id !== $user->id){
        return redirect('/')->with('error', 'Unauthorized Action');
      }
      $user->name = $request->input('name');
      $user->lname = $request->input('lname');
      $user->country = $request->input('country');
      $user->city = $request->input('city');
      $user->email = $request->input('email');
      if (!$request->input('password') == ""){
        $request->validate([
          'password' => 'string|min:8|confirmed'
        ]);
        $user->password = bcrypt($request->input('password'));
      }
      $user->save();
      return redirect('home')->with("user", $user);
  }
}
