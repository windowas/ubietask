@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-heading">Update</div>
              <div class="panel-body">
                {!! Form::open(['action' => ['HomeController@update', $user->id], 'method' => 'POST']) !!}
                      <div class="form-group">
                          {{Form::label('name', 'Name')}}
                          {{Form::text('name', $user->name, ['class' => 'form-control'])}}
                      </div>
                      <div class="form-group">
                        {{Form::label('lname', 'Last Name')}}
                        {{Form::text('lname', $user->lname, ['class' => 'form-control'])}}
                      </div>
                      <div class="form-group">
                          {{Form::label('country', 'Country')}}
                          {{Form::select('country', array (
                            'Australia' =>'Australia',
                            'Belgium' =>'Belgium',
                            'China' =>'China',
                            'Denmark' =>'Denmark',
                            'Finland' =>'Finland',
                            'Frace' =>'Frace',
                            'Germany' =>'Germany',
                            'Italy' =>'Italy',
                            'Japan' =>'Japan',
                            'Lithuania' =>'Lithuania',
                            'Marocco' =>'Marocco',
                            'Netherlands' =>'Netherlands',
                            'Norway' =>'Norway',
                            'Poland' =>'Poland',
                            'Portugal' =>'Portugal',
                            'Russia' =>'Russia',
                            'Sweden' =>'Sweden',
                            'Spain' =>'Spain',
                            'Ukraine' =>'Ukraine',
                            'United Kingdom' =>'United Kingdom',
                            'USA' =>'USA',
                          ), $user->country, ['class' => 'form-control'])}}
                      </div>
                      <div class='form-group'>
                          {{Form::label('city', 'City')}}
                          {{Form::select('city', array(
                          ), $user->city, ['class' => 'form-control', 'value' => $user->city])}}
                      </div>

                      <div class="form-group">
                          {{Form::label('email', 'Email')}}
                          {{Form::email('email', $user->email, ['class' => 'form-control'])}}
                      </div>
                      <div class="form-group">
                          {{Form::label('password', 'Password')}}
                          {{Form::password('password', ['class' => 'form-control'])}}
                      </div>
                      <div class="form-group">
                          {{Form::label('password_confirmation', 'Confirm Password')}}
                          {{Form::password('password_confirmation', ['class' => 'form-control'])}}
                      </div>
                      <div class="form-group">
                          {{Form::hidden('_method', 'PUT')}}
                          {{Form::submit('Atnaujinti', ['class' => 'btn btn-primary'])}}
                          {!! Form::close() !!}
                    <a class="btn pull-right" href={{url()->previous()}}><button type="button" name="button">Atšaukti</button></a>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
