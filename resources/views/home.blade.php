@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <table class="table">
                  <tr>
                    <td>Name</td>
                    <td>{{$user->name}}</td>
                  </tr>
                  <tr>
                    <td>Last Name</td>
                    <td>{{$user->lname}}</td>
                  </tr>
                  <tr>
                    <td>Country</td>
                    <td>{{$user->country}}</td>
                  </tr>
                  <tr>
                    <td>City</td>
                    <td>{{$user->city}}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{{$user->email}}</td>
                  </tr>
                </table>
            </div>
          <a href="/user/{{$user->id}}/edit"><button id="update" class="btn btn-warning" type="button" name="button">Edit</button></a>        
      </div>
    </div>
</div>

@endsection
