Ubie užduotis:  

Parašyti aplikaciją, kurioje būtų galima:  

užsiregistruoti  
prisijungti  
prisijungus pakeisti duomenis paskyroje  
atsijungti  

Bonus užduotis:  
  
prisijungus ir atsijungus saugoti šio veiksmo datą  
lentelė su datos ir vieksmo (prisijungimas/atsijungimas) stulpeliais  
  
Panaudotas Laravel karkasas dėl MVC(model-view-controller) patogumo susieti .php dokumentus bei vartotojų kūrimo funkcijų. JQuery pagalba parenkami miestai šalims.  
Vartotojo paskutinio prisijungimo bei atsijungimo data saugoma duomenų bazėje.  
  
Norint paleisti aplikaciją, reikia nusiklonuoti repozitoriją.  
Įsidiegti MAMP (My Apache - MySQL - PHP) aplikaciją. Parsisiųti galima čia: https://www.mamp.info/en/  
Paleidus MAMP aplikaciją, paspausti "Preferences", tada prisirinkti "Web Server" tabuliaciją. Spausti mygtuką "Select", atsiradusiame lange pasirinkti nuklonuotos repozitorijos 'public' bylą.  
MAMP aplikacijoje spaudžiame "Open Start Page". Atsidarusiame naršyklės lange renkamės "Tools", spaudžiame "phpMyAdmin". Susikuriame naują duomenų bazę. Į sukurtą duomenų bazę  
eksportuojame ubie.sql dokumentą iš nuklonuotos repozitorijos.  
Nuklonuotoje repozitorijoje .env dokumente pakeičiame nustatymus atitinkamus savo MySQL klientui:  
DB_PORT=(MAMP MySQL portas)  
DB_DATABASE=(sukurtos duomenų bazės pavadinmas)  
DB_USERNAME=(MySQL root teises turintis vartotojas)  
DB_PASSWORD=(root vartotojo slaptažodis)  
Naršyklės lange įrašome localhost

