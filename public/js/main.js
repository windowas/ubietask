// ==================== Countries cities =================
var cityAU = ['Cannbera', 'Sidney'];
var cityBE = ['Brussel', "Antwerp"];
var cityCH = ['Beijin', 'Shanghai'];
var cityDE = ['Copenhagen', 'Esbjerg', "Odense"];
var cityFN = ['Espoo' ,'Helsinki'];
var cityFR = ['Marseille', 'Paris'];
var cityGR = ['Berlin', 'Frankfurt', 'Hamburg'];
var cityIT = ['Milan', 'Roma'];
var cityJP = ['Nagoya', 'Okazaki', 'Tokyo'];
var cityLT = ['Marijampole', 'Prienai', 'Utena'];
var cityMR = ['Casablanca', 'Rabat'];
var cityNL = ['Amsterdam', 'Roterdam'];
var cityNO = ['Bergen', 'Oslo'];
var cityPL = ['Poznan', 'Warsaw'];
var cityPO = [ 'Alfena', 'Lisbon'];
var cityRU = ['Mascow', 'Saint Petersburg'];
var citySP = ['Barselona', 'Madrid'];
var citySW = ['Stockholm', 'Vadstena'];
var cityUA = ['Kyiv', 'Odesa'];
var cityUK = ['Edingburg', 'London'];
var cityUS = ['Los Angeles', 'New York'];
// ========================== register cities =======================
var country = $('#regcountry');
var regcity = $('#regcity');
country.on("change", function (){
  $('#regcity').empty();
  $('#regcity')
    .append("<option disabled selected value> -- select a city -- </option>");
  selectCities(this.value, regcity);
});
// ==================== Countries cities Update =================
var countryupdate = $('#country');
var currentcountry = countryupdate.find(":selected").text();
var cityID = $("#city");

selectCities(currentcountry, cityID);

countryupdate.on("change", function(){
  $('#city').empty();
  $('#city')
    .append("<option disabled selected value> -- select an city -- </option>");
    selectCities(this.value, cityID);
});

// =============================cities depend on countries =============
function selectCities(country, id) {
  switch(country){
    case("Australia"):
    $.each(cityAU, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Belgium"):
    $.each(cityBE, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("China"):
    $.each(cityCH, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Denmark"):
    $.each(cityDE, function(key, value) {
      id
      .append($('<option>', { value : value})
      .text(value));
    });
    break;
    case("Finland"):
    $.each(cityFN, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Frace"):
    $.each(cityFR, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Germany"):
    $.each(cityGR, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Italy"):
    $.each(cityIT, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Japan"):
    $.each(cityJP, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Lithuania"):
    $.each(cityLT, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Marocco"):
    $.each(cityMR, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Netherlands"):
    $.each(cityNL, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Norway"):
    $.each(cityNO, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Poland"):
    $.each(cityPL, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Portugal"):
    $.each(cityPO, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Russia"):
    $.each(cityRU, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Spain"):
    $.each(citySP, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Sweden"):
    $.each(citySW, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("Ukraine"):
    $.each(cityUA, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("United Kingdom"):
    $.each(cityUK, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
    case("USA"):
    $.each(cityUS, function(key, value) {
      id
      .append($('<option>', { value : value })
      .text(value));
    });
    break;
  }}
